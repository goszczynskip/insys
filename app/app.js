(function () {
  'use strict';

// Declare app level module which depends on views, and components
  angular.module('mainApp', [
    'ui.router',
    'layout'
  ]).config(['$urlRouterProvider', function ($urlRouterProvider) {
      $urlRouterProvider.when('', '/');
      $urlRouterProvider.otherwise("/profile");
    }])
    .run(function ($rootScope) {
      $rootScope.$on("$stateChangeError", console.log.bind(console));
    });
})();

