/**
 * Created by Piotr on 19.03.2016.
 */
(function () {

  angular.module('layout', [
    'view.gallery',
    'view.profile',
    'layout.navigation',
  ])
    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider.state('main',{
        url: '',
        abstract:true,
        views: {
          'header': {
            templateUrl: 'modules/layout.header/header.tpl.html'
          },
          'navigation': {
            templateUrl: 'modules/layout.navigation/navigation.tpl.html',
            controller: 'NavigationController'
          }
        }
      })
    }]);
})();