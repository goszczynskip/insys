/**
 * Created by Piotr on 19.03.2016.
 */
(function () {
  var NavigationController = function($scope,$location){
    $scope.isCollapsed = true;
    $scope.getClass = function (path, baseClass) {
      return ($location.path().substr(0, path.length) === path) ? baseClass+'--active' : '';
    }
  }

  var fixCollapse = function($animate) {
    return function(scope, element, attrs) {
      scope.$watch(attrs.fixCollapse, function(newVal) {
        if (newVal) {
          $animate.addClass(element, "in")
        } else {
          $animate.removeClass(element, "in")
        }
      })
    }
  }


  angular.module('layout.navigation', [
    'ui.bootstrap'
  ])
    .controller('NavigationController', ['$scope','$location',NavigationController])
    .directive("hideMe", ['$animate', fixCollapse])
})();