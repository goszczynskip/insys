/**
 * Created by Piotr on 16.03.2016.
 */
(function () {
  'use strict';
  var GalleryController = function ($scope, flickrPhotos) {
    flickrPhotos.load({tags: "marilynmonroe",}).$promise.then(function successCallback(result){
      $scope.photos = result.items;
    },function errorCallback(result){
      console.log(result);
    }
    );
  };


  var module = angular.module('view.gallery', ['ngResource'])
    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider.state('main.gallery', {
        url: '/gallery',
        views: {
          'container@': {
            templateUrl: 'modules/view.gallery/gallery.tpl.html',
            controller: 'GalleryController'
          }
        }
      });
    }]);
  module.factory('flickrPhotos', ['$resource', function flickrPhotosFactory($resource) {
    return $resource('http://api.flickr.com/services/feeds/photos_public.gne', {
      format: 'json',
      jsoncallback: 'JSON_CALLBACK'
    }, {'load': {'method': 'JSONP'}});
  }]);
  module.controller('GalleryController', ['$scope','flickrPhotos', GalleryController]);


})()
