'use strict';

angular.module('view.profile', ['ui.router'])

  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('main.profile', {
      url: '/profile',
      views: {
        'container@': {
          templateUrl: 'modules/view.profile/profile.tpl.html',
          controller: 'ProfileController',
          controllerAs: 'profile'
        }
      }
    });
  }])

  .controller('ProfileController', [function () {

  }]);